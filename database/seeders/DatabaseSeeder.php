<?php

namespace Database\Seeders;

use App\Department;
use App\Employee;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Department::factory(10)->create();
        Employee::factory(50)->create();
    }
}
