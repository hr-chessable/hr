<?php

namespace Database\Factories;

use App\Employee;
use App\Repositories\DepartmentsRepository;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class EmployeeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Employee::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $departments = resolve(DepartmentsRepository::class)->getAll();

        $randomDepartment = Arr::get($departments, random_int(0, count($departments) - 1));

        return [
            'name' => $this->faker->firstName,
            'lastName' => $this->faker->lastName,
            'salary' => $this->faker->numberBetween(20000, 70000),
            'department_id' => $randomDepartment->id
        ];
    }
}
