const mix = require('laravel-mix');

mix.react('resources/js/hr/app.js', 'public/js/hr/')
   .sass('resources/sass/hr/app.scss', 'public/css/hr/');
