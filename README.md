## Introduction
This is a simple HR app to manage departments and employees.

The data is provided by an API built in Laravel and the frontend is built using React, and living both in a Laravel Application.

## Installation steps

1. Clone the project
```
$ git clone git@gitlab.com:hr-chessable/hr.git
```
2. Start docker from the app path
```
$ cd hr
$ docker-compose up -d --force-recreate --build
```
3. Enter the application container and get composer packages and run migrations to create database tables
```
$ docker exec -it hr-app /bin/bash
$ composer install
$ php artisan migrate
```
You can access the application in http://localhost
