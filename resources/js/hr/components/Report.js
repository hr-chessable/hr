import React from 'react';

class Report extends React.Component {

    constructor(props) {
        super(props);

        //Initialize the state in the constructor
        this.state = {
            items: [],
            endpoint: '/api/reports/' + props.id
        }
    }

    componentDidMount() {
        fetch(this.state.endpoint)
            .then(response => {
                return response.json();
            })
            .then(items => {
                this.setState({ items });
            });
    }

    renderReport() {
        return this.state.items.map(item => {
            return (
                <tr key={item.id}>
                    <th key={item.id + "col-0"}>{item[this.props.fields[0][Object.keys(this.props.fields[0])]]}</th>
                    <td key={item.id + "col-1"}>{item[this.props.fields[1][Object.keys(this.props.fields[1])]]}</td>
                    <td key={item.id + "col-2"}>{item[this.props.fields[2][Object.keys(this.props.fields[2])]]}</td>
                </tr>
            );
        })
    }

    render() {
        return (
            <div className="container">
                <h2>{ this.props.title.toUpperCase() }</h2>
                <table className="table table-striped">
                    <thead className="thead-dark">
                    <tr key="heading">
                        <th key="heading-0" scope="col">{Object.keys(this.props.fields[0])[0]}</th>
                        <th key="heading-1" scope="col">{Object.keys(this.props.fields[1])[0]}</th>
                        <th key="heading-2" scope="col">{Object.keys(this.props.fields[2])[0]}</th>
                    </tr>
                    </thead>
                    <tbody>
                        { this.renderReport() }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Report;
