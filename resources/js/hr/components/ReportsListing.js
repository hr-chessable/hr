import React from 'react';
import { BrowserRouter as Router, Link } from 'react-router-dom'

class ReportsListing extends React.Component {
    render() {
        return <div className="container">
            <div className="row text-center">
                <h1>Reports</h1>
            </div>
            <div className="row">
                <ul>
                    <li><Link to={'/reports/highest-salary-by-department'}>Highest salary by department</Link></li>
                    <li><Link to={'/reports/high-salary-department'}>Departments with more than two employees that earn over 50k.</Link></li>
                </ul>
            </div>
        </div>
    }
}

export default ReportsListing;
