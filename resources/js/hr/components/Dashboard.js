import React from 'react';
import ItemsList from "../components/ItemsList";
import Report from "./Report";


class Dashboard extends React.Component {
    render() {
        return <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <Report
                                id="salary-by-dpt"
                                title="Highest salary by department"
                                fields={[
                                      {'#': 'department_id'}
                                    , {'Department': 'department_name'}
                                    , {'Max. Salary': 'max_salary'}
                                ]}
                            />
                        </div>
                        <div className="col-md-6">
                            <Report
                                id="highest-salary-dpt"
                                title="More than 50K salary departments"
                                fields={[
                                      {'#': 'department_id'}
                                    , {'Department': 'name'}
                                    , {'Employees with > 50K': 'num_employees'}
                                ]}
                            />
                        </div>
                    </div>
                </div>
    }
}

export default Dashboard;
