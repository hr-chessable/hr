import React from 'react';
import EmployeeForm from "./Forms/EmployeeForm";
import DepartmentForm from "./Forms/DepartmentForm";
import {Link} from "react-router-dom";

class ItemsList extends React.Component {

    constructor(props) {
        super(props);

        //Initialize the state in the constructor
        this.state = {
            items: [],
            endpoint: '/api/' + props.entity
        }
    }

    componentDidMount() {
        fetch(this.state.endpoint)
            .then(response => {
                return response.json();
            })
            .then(items => {
                items = items.data;
                this.setState({ items });
            });
    }

    renderItems() {
        return this.state.items.map(item => {
            return (
                <tr key={item.id}>
                    <th key={item.id + "col-0"}>{item[this.props.fields[0][Object.keys(this.props.fields[0])]]}</th>
                    <td key={item.id + "col-1"}>{item[this.props.fields[1][Object.keys(this.props.fields[1])]]}</td>
                    {typeof this.props.fields[2]  !== 'undefined' &&
                    <td key={item.id + "col-2"}>{item[this.props.fields[2][Object.keys(this.props.fields[2])]]}</td>
                    }
                    {typeof this.props.fields[3]  !== 'undefined' &&
                    <td key={item.id + "col-3"}>{item[this.props.fields[3][Object.keys(this.props.fields[3])]]}</td>
                    }
                </tr>
            );
        })
    }

    render() {
        return (
            <div className="container">

                <div className="container-xl">
                    <div className="table-responsive">
                        <div className="table-wrapper">
                            <div className="table-title">
                                <div className="row">
                                    <div className="col-sm-6">
                                        <h2>Manage <b>{ this.props.entity.toUpperCase() }</b></h2>
                                    </div>
                                </div>
                            </div>
                            <table className="table table-striped mt-2">
                                <thead className="thead-dark">
                                <tr>
                                    <th colSpan={5}>
                                        <Link className="nav-link" to={'/'+this.props.entity+'/create'}>
                                            <button className="btn btn-success">Add New</button>
                                        </Link>
                                    </th>
                                </tr>
                                <tr key={"row-1"}>
                                    {/*We build the table headings with fields prop*/}
                                    <th key="heading-0" scope="col">{Object.keys(this.props.fields[0])[0]}</th>
                                    <th key="heading-1" scope="col">{Object.keys(this.props.fields[1])[0]}</th>
                                    {typeof this.props.fields[2] !== 'undefined' &&
                                    <th key="heading-2" scope="col">{Object.keys(this.props.fields[2])[0]}</th>
                                    }
                                    {typeof this.props.fields[3] !== 'undefined' &&
                                    <th key="heading-3" scope="col">{Object.keys(this.props.fields[3])[0]}</th>
                                    }
                                </tr>
                                </thead>
                                <tbody>
                                { this.renderItems() }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        );
    }
}

export default ItemsList;
