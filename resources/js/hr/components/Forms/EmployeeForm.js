import React from 'react';

class EmployeeForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            fields: {},
            errors: {},
            departmentOptions: [],
        }
    }

    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        const regexpNumber = new RegExp(`^-?[0-9]*$`);

        if (!fields["name"]) {
            formIsValid = false;
            errors["name"] = "This field is required";
        }

        if (!fields["salary"]) {
            formIsValid = false;
            errors["salary"] = "This field is required";
        } else if(!regexpNumber.test(fields["salary"])) {
            formIsValid = false;
            errors["salary"] = "Salary must be a number. E.g 15000, 50000";
        }

        this.setState({
            errors: errors,
            validated: true
        });

        return formIsValid;
    }

    submit(e) {
        e.preventDefault();
        const data = new FormData(e.target);

        if (this.handleValidation()) {
            fetch('/api/employees', {
                method: 'POST',
                body: data,
            }).then(response => {
                this.props.history.push('/employees');
            })
        }
    }

    renderDepartmentOptions() {
        return this.state.departmentOptions.map(department => {
            return (<option key={department.id} value={department.id}>{department.name}</option>);
        });
    }

    componentDidMount() {
        fetch('/api/departments')
            .then(response => {
                return response.json();
            })
            .then(departmentOptions => {
                departmentOptions = departmentOptions.data;
                this.setState({departmentOptions});
            });
    }

    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({fields});
    }

    render() {
        return (
            <div className="container">
                <div className="form">
                    <form
                        name="createEmployee"
                        className={`needs-validation ${this.state.validated === true ? 'was-validated' : ''}`}
                        onSubmit={this.submit.bind(this)}
                        noValidate
                    >
                        <h2>Create Employee</h2>
                        <div className="form-row">
                            <div className="col-md-12">
                                <label htmlFor="name">Name *</label>
                                <input
                                    name="name"
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    placeholder="Name"
                                    value={this.state.salary}
                                    onChange={this.handleChange.bind(this, "name")}
                                    required
                                />
                                {this.state.errors['name'] ?
                                    <div className="invalid-feedback">{this.state.errors['name']}</div> : ""}
                            </div>
                            <div className="col-md-12 mt-4">
                                <label htmlFor="salary">Salary *</label>
                                <input
                                    name="salary"
                                    type="number"
                                    className="form-control"
                                    id="salary"
                                    placeholder="Salary"
                                    value={this.state.salary}
                                    onChange={this.handleChange.bind(this, "salary")}
                                    required
                                />
                                {this.state.errors['salary'] ?
                                    <div className="invalid-feedback">{this.state.errors['salary']}</div> : ""}
                            </div>
                            <div className="col-md-12 mt-4">
                                <label htmlFor="department">Department *</label>
                                <select name="department_id" className="form-control" id="department">
                                    {this.renderDepartmentOptions()}
                                </select>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="col-md-12 mt-4">
                                <button className="btn btn-success" id="submit" value="Submit">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default EmployeeForm;
