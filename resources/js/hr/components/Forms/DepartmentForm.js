import React from 'react';

class DepartmentForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            fields: {},
            errors: {}
        }
    }

    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        if (!fields["name"]) {
            formIsValid = false;
            errors["name"] = "Cannot be empty";
        }

        this.setState({
            errors: errors,
            validated: true
        });

        return formIsValid;
    }

    submit(e) {
        e.preventDefault();
        const data = new FormData(e.target);

        if (this.handleValidation()) {
            fetch('/api/departments', {
                method: 'POST',
                body: data,
            }).then(response => {
                this.props.history.push('/departments');
            })
        }
    }

    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({fields});
    }

    render() {
        return (
            <div className="container">
                <div className="form">
                    <form
                        name="createDepartment"
                        className={`needs-validation ${this.state.validated === true  ? 'was-validated' : ''}`}
                        onSubmit={this.submit.bind(this)}
                        noValidate
                    >
                        <h2>Create Departments</h2>
                        <div className="form-row">
                            <div className="col-md-4 mb-3">
                                <label htmlFor="name">Name *</label>
                                <input
                                    name="name"
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    placeholder="Name"
                                    value={this.state.salary}
                                    onChange={this.handleChange.bind(this, "name")}
                                    required
                                />
                                {this.state.errors['name'] ? <div className="invalid-feedback">Please choose a name</div> : ""}
                            </div>
                        </div>
                        <div className="form-group">
                             <button className="btn btn-success" id="submit" value="Submit">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default DepartmentForm;









