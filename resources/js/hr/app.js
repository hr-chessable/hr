require('../bootstrap');

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, NavLink } from 'react-router-dom'
import Routes from './routes';

class App extends React.Component {
    render() {
        return (
            <Router>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                    <button
                        className="navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbarTogglerDemo01"
                        aria-controls="navbarTogglerDemo01"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                    >
                        <span className="navbar-toggler-icon"/>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
                        <ul className="navbar-nav">
                            <li className="nav-item"><NavLink exact={true} className="nav-link" to={''}>Dashboard</NavLink></li>
                            <li className="nav-item"><NavLink className="nav-link" to={'/departments'}>Departments</NavLink></li>
                            <li className="nav-item"><NavLink className="nav-link" to={'/employees'}>Employees</NavLink></li>
                            <li className="nav-item"><NavLink className="nav-link" to={'/reports'}>Reports</NavLink></li>
                        </ul>
                    </div>
                </nav>
                <Routes />
            </Router>
    )
    }
}

export default App;

if (document.getElementById('app')) {
    ReactDOM.render(
    <App />
        , document.getElementById('app'));
}
