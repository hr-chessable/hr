import React from 'react';
import {Link, Route, Switch} from 'react-router-dom'
import Dashboard from './components/Dashboard';
import ItemsList from "./components/ItemsList";
import ReportsListing from "./components/ReportsListing";
import Report from "./components/Report";
import DepartmentForm from "./components/Forms/DepartmentForm";
import EmployeeForm from "./components/Forms/EmployeeForm";

class Routes extends React.Component {
    render() {
        return (
            <Switch>
                <Route exact path='/' component={Dashboard} />
                <Route path='/departments/create' component={DepartmentForm} />
                <Route path='/departments' component={
                    props => <ItemsList entity="departments" fields={[{'ID': 'id'}, {'Name': 'name'}]} />
                } />
                <Route path='/employees/create' component={EmployeeForm} />
                <Route path='/employees' component={
                    props => <ItemsList
                        entity="employees"
                        fields={[
                              {'ID': 'id'}
                            , {'Name': 'name'}
                            , {'Salary': 'salary'}
                            , {'Department': 'department'}
                        ]}
                    />
                } />
                <Route
                    path='/reports/highest-salary-by-department'
                    component={props => <Report
                        id="salary-by-dpt"
                        title="Highest salary by department"
                        fields={[
                              {'Department ID': 'department_id'}
                            , {'Department Name': 'department_name'}
                            , {'Max. Salary': 'max_salary'}
                        ]}
                    />}
                />
                <Route
                    path='/reports/high-salary-department'
                    component={props => <Report
                        id="highest-salary-dpt"
                        title="Departments with more than two employees that earn over 50k."
                        fields={[
                            {'Department ID': 'department_id'}
                            , {'Department Name': 'name'}
                            , {'Num. Employees': 'num_employees'}
                        ]}
                    />}
                />
                <Route path='/reports' component={ReportsListing} />
            </Switch>
        )
    }
}

export default Routes;
