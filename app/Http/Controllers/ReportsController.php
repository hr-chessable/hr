<?php

namespace App\Http\Controllers;

use App\Repositories\ReportsRepository;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    private ReportsRepository $repository;

    public function __construct(ReportsRepository $report)
    {
        $this->repository = $report;
    }

    public function getSalaryByDepartmentReport()
    {
        return $this->repository->getDepartmentsWithHighSalaryReport();
    }

    public function getDepartmentsWithMoreThanTwoEmployeesWithMoreThan50K()
    {
        return $this->repository->getDepartmentsWithMoreThanTwoEmployeesWithMoreThan50K();
    }
}
