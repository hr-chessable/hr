<?php

namespace App\Providers;

use App\Repositories\BannerRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\DepartmentsRepository;
use App\Repositories\Eloquent\EloquentBannerRepository;
use App\Repositories\Eloquent\EloquentCategoryRepository;
use App\Repositories\Eloquent\EloquentMoodRepository;
use App\Repositories\Eloquent\EloquentNotificationRepository;
use App\Repositories\Eloquent\EloquentPodcastRepository;
use App\Repositories\Eloquent\EloquentPublisherRepository;
use App\Repositories\Eloquent\EloquentRecommendationRepository;
use App\Repositories\Eloquent\EloquentScheduleRepository;
use App\Repositories\Eloquent\EloquentSeriesRepository;
use App\Repositories\Eloquent\EloquentShowRepository;
use App\Repositories\Eloquent\EloquentStationRepository;
use App\Repositories\Eloquent\EloquentStreamRepository;
use App\Repositories\Eloquent\EloquentUserDeviceRepository;
use App\Repositories\Eloquent\EloquentUserFavouriteRepository;
use App\Repositories\Eloquent\EloquentUserListeningHistoryRepository;
use App\Repositories\Eloquent\EloquentUserNotificationPreferenceRepository;
use App\Repositories\Eloquent\EloquentUserRepository;
use App\Repositories\EmployeesRepository;
use App\Repositories\File\FileUploadRepository;
use App\Repositories\FileUploadRepository as FileRepository;
use App\Repositories\MoodRepository;
use App\Repositories\MySQL\DeparmentsMysqlRepository;
use App\Repositories\MySQL\EmployeesMysqlRepository;
use App\Repositories\MySQL\ReportsMysqlRepository;
use App\Repositories\NotificationRepository;
use App\Repositories\PodcastRepository;
use App\Repositories\PublisherRepository;
use App\Repositories\RecommendationRepository;
use App\Repositories\ReportsRepository;
use App\Repositories\ScheduleRepository;
use App\Repositories\SeriesRepository;
use App\Repositories\ShowRepository;
use App\Repositories\StationRepository;
use App\Repositories\StreamRepository;
use App\Repositories\Testing\TestingStreamRepository;
use App\Repositories\UserDeviceRepository;
use App\Repositories\UserFavouriteRepository;
use App\Repositories\UserListeningHistoryRepository;
use App\Repositories\UserNotificationPreferenceRepository;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind(EmployeesRepository::class, EmployeesMysqlRepository::class);
        app()->bind(DepartmentsRepository::class, DeparmentsMysqlRepository::class);
        app()->bind(ReportsRepository::class, ReportsMysqlRepository::class);
    }

}
