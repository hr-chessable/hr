<?php


namespace App\Domain;


class Department
{
    public string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }
}
