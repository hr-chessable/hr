<?php


namespace App\Domain;


class Employee
{
    public string $name;
    public int $salary;

    public function __construct(string $name, int $salary)
    {
        $this->name = $name;
        $this->salary = $salary;
    }
}
