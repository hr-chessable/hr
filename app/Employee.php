<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public int $id;
    public string $name;
    public int $salary;
    public int $department_id;

    public static function build(array $data): Employee
    {
        $department = new self;
        $department->id = $data['id'];
        $department->name = $data['name'];
        $department->salary = $data['salary'];
        $department->department_id = $data['department_id'];

        return $department;
    }

}
