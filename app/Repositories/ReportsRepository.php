<?php


namespace App\Repositories;


interface ReportsRepository
{
    public function getDepartmentsWithHighSalaryReport(): array;
    public function getDepartmentsWithMoreThanTwoEmployeesWithMoreThan50K(): array;
}
