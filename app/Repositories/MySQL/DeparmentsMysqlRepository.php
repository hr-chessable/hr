<?php


namespace App\Repositories\MySQL;


use App\Department;
use App\Repositories\DepartmentsRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class DeparmentsMysqlRepository implements DepartmentsRepository
{

    public function getAll(): array
    {
        return DB::select('select * from departments');
    }

    public function find(int $id): Department
    {
        $departmentData = DB::select('select * from departments where id = ?', [$id]);
        return Department::build(
            [
                'id' => $departmentData[0]->id,
                'name' => $departmentData[0]->name
            ]
        );
    }

    public function create(array $params): Department
    {
        DB::select('insert into departments (name) values (?)', [Arr::get($params, 'name')]);

        $newId = DB::getPdo()->lastInsertId();

        return $this->find($newId);
    }
}
