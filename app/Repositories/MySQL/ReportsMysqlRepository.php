<?php


namespace App\Repositories\MySQL;

use App\Repositories\ReportsRepository;
use Illuminate\Support\Facades\DB;

class ReportsMysqlRepository implements ReportsRepository
{
    public function getDepartmentsWithMoreThanTwoEmployeesWithMoreThan50K(): array
    {
        return DB::select('
            SELECT
                   e.department_id,
                   d.name,
                   Count(*) as num_employees
            FROM employees as e
            JOIN departments as d ON d.id = e.department_id
            WHERE e.salary >= 50000
            GROUP BY e.department_id
            HAVING num_employees > 1;
        ');
    }

    public function getDepartmentsWithHighSalaryReport(): array
    {
        return DB::select('
            SELECT
                   departments.id as department_id,
                   departments.name as department_name,
                   COALESCE(MAX(salary),0) as max_salary
            FROM departments
            LEFT JOIN employees
                ON departments.id = employees.department_id
            GROUP BY departments.id
            ORDER BY max_salary DESC
        ');
    }
}
