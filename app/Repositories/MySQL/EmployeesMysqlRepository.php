<?php


namespace App\Repositories\MySQL;


use App\Department;
use App\Employee;
use App\Repositories\EmployeesRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class EmployeesMysqlRepository implements EmployeesRepository
{

    public function getAll(): array
    {
        return DB::select('
            SELECT e.id, e.name, d.name as department, e.department_id, e.salary
            FROM employees as e
            JOIN departments as d
                ON d.id = e.department_id
            ORDER BY e.id ASC
        ');
    }

    public function find(int $id): Employee
    {
        $data = DB::select('select * from employees where id = ?', [$id]);

        return Employee::build(
            [
                'id' => $data[0]->id,
                'name' => $data[0]->name,
                'salary' => $data[0]->salary,
                'department_id' => $data[0]->department_id,
            ]
        );
    }

    public function create(array $params): Employee
    {
        DB::select('insert into employees (name, salary, department_id) values (?,?,?)', [
            Arr::get($params, 'name'), Arr::get($params, 'salary'), Arr::get($params, 'department_id'),
        ]);

        $newId = DB::getPdo()->lastInsertId();

        return $this->find($newId);
    }

}
