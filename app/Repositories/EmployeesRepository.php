<?php


namespace App\Repositories;


interface EmployeesRepository
{
    public function getAll(): array;
}
