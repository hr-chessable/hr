<?php


namespace App\Repositories;


use App\Department;

interface DepartmentsRepository
{
    public function getAll(): array;
    public function create(array $params): Department;
}
