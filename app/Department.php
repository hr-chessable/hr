<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public int $id;
    public string $name;

    public static function build(array $data): Department
    {
        $department = new self;
        $department->name = $data['name'];
        $department->id = $data['id'];

        return $department;
    }

    public function getId(): int
    {
        return $this->id;
    }

}

