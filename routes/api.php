<?php

use App\Http\Controllers\DepartmentsController;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\ReportsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('employees', EmployeesController::class);
Route::apiResource('departments', DepartmentsController::class);


Route::prefix('/reports')->group(function () {
    Route::get('salary-by-dpt', [ReportsController::class, 'getSalaryByDepartmentReport'] );
    Route::get('highest-salary-dpt', [ReportsController::class, 'getDepartmentsWithMoreThanTwoEmployeesWithMoreThan50K'] );
});
