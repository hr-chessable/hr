<?php

namespace Tests\Repositories;

use App\Repositories\DepartmentsRepository;
use App\Repositories\EmployeesRepository;
use App\Repositories\ReportsRepository;
use Tests\TestCase;

class ReportsRepositoryTest extends TestCase
{

    public function testMoreThanTwoEmployeesWith50KSalaryDepartmentsReport(): void
    {
        # Requirement 1: List just those departments that have more than two employees that earn over 50k

        $departmentsRepository = resolve(DepartmentsRepository::class);
        $departmentA = $departmentsRepository->create(['name' => 'Department A']);
        $departmentB = $departmentsRepository->create(['name' => 'Department B']);
        $departmentsRepository->create(['name' => 'Department C']);

        $employeesRepository = resolve(EmployeesRepository::class);
        $employeesRepository->create(['name' => 'Employee A', 'salary' => 51000, 'department_id' => $departmentA->getId()]);
        $employeesRepository->create(['name' => 'Employee B', 'salary' => 40000, 'department_id' => $departmentA->getId()]);
        $employeesRepository->create(['name' => 'Employee C', 'salary' => 50000, 'department_id' => $departmentA->getId()]);
        $employeesRepository->create(['name' => 'Employee D', 'salary' => 49000, 'department_id' => $departmentB->getId()]);
        $employeesRepository->create(['name' => 'Employee E', 'salary' => 40000, 'department_id' => $departmentB->getId()]);
        $employeesRepository->create(['name' => 'Employee F', 'salary' => 50000, 'department_id' => $departmentB->getId()]);

        $reportsRepository = resolve(ReportsRepository::class);
        $reportResult = $reportsRepository->getDepartmentsWithMoreThanTwoEmployeesWithMoreThan50K();

        # Requirement 1: List just those departments that have more than two employees that earn over 50k
        self::assertCount(1, $reportResult);

        $employeesRepository->create(['name' => 'Employee G', 'salary' => 52000, 'department_id' => $departmentB->getId()]);
        $employeesRepository->create(['name' => 'Employee H', 'salary' => 51000, 'department_id' => $departmentB->getId()]);
        $employeesRepository->create(['name' => 'Employee I', 'salary' => 55000, 'department_id' => $departmentB->getId()]);

        $reportResult = $reportsRepository->getDepartmentsWithMoreThanTwoEmployeesWithMoreThan50K();
        self::assertCount(2, $reportResult);

        self::assertObjectHasAttribute('department_id', $reportResult[0]);
        self::assertObjectHasAttribute('name', $reportResult[0]);
        self::assertObjectHasAttribute('num_employees', $reportResult[0]);

        self::assertEquals(2, data_get($reportResult, '0.num_employees'));
        self::assertEquals(4, data_get($reportResult, '1.num_employees'));
    }

    public function testDepartmentsWithHighestSalaryReport(): void
    {
        # Requirement 1: Show all departments along with the highest salary within each department
        # Requirement 2: A department with no employees should show 0 as the highest salary.

        $departmentsRepository = resolve(DepartmentsRepository::class);
        $departmentA = $departmentsRepository->create(['name' => 'Department A']);
        $departmentB = $departmentsRepository->create(['name' => 'Department B']);
        $departmentsRepository->create(['name' => 'Department C']);

        $employeesRepository = resolve(EmployeesRepository::class);
        $employeesRepository->create(['name' => 'Employee A', 'salary' => 105000, 'department_id' => $departmentA->getId()]);
        $employeesRepository->create(['name' => 'Employee B', 'salary' => 40000, 'department_id' => $departmentA->getId()]);
        $employeesRepository->create(['name' => 'Employee B', 'salary' => 99000, 'department_id' => $departmentA->getId()]);
        $employeesRepository->create(['name' => 'Employee F', 'salary' => 55000, 'department_id' => $departmentB->getId()]);
        $employeesRepository->create(['name' => 'Employee F', 'salary' => 12500, 'department_id' => $departmentB->getId()]);

        $reportsRepository = resolve(ReportsRepository::class);
        $reportResult = $reportsRepository->getDepartmentsWithHighSalaryReport();

        # Requirement 1: Show all departments along with the highest salary within each department
        self::assertEquals(105000, data_get($reportResult, '0.max_salary'));
        self::assertEquals("Department A", data_get($reportResult, '0.department_name'));
        self::assertEquals(55000, data_get($reportResult, '1.max_salary'));
        self::assertEquals("Department B", data_get($reportResult, '1.department_name'));

        # Requirement 2: A department with no employees should show 0 as the highest salary.
        self::assertSame("0", data_get($reportResult, '2.max_salary'));
        self::assertSame("Department C", data_get($reportResult, '2.department_name'));

    }
}
