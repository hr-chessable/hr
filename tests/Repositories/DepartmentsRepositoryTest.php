<?php

namespace Tests\Repositories;

use App\Repositories\DepartmentsRepository;
use Tests\TestCase;

class DepartmentsRepositoryTest extends TestCase
{

    public function testDepartmentCanBeCreated(): void
    {
        # Requirement 1: It should be possible to add departments and employees
        $departmentsRepository = resolve(DepartmentsRepository::class);
        $departmentsRepository->create(['name' => 'Department A']);
        $departmentsRepository->create(['name' => 'Department A']);

        $departments = $departmentsRepository->getAll();

        self::assertCount(2, $departments);
    }

}
