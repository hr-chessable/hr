<?php

namespace Tests\Repositories;

use App\Repositories\DepartmentsRepository;
use App\Repositories\EmployeesRepository;
use Tests\TestCase;

class EmployeesRepositoryTest extends TestCase
{

    public function testDepartmentCanBeCreated(): void
    {
        # Requirement 1: It should be possible to add employees and employees
        $departmentRepository = resolve(DepartmentsRepository::class);
        $departmentA = $departmentRepository->create(['name' => 'Department A']);

        $employeesRepository = resolve(EmployeesRepository::class);
        $employeesRepository->create(['name' => 'Employee A', 'salary' => 51000, 'department_id' => $departmentA->getId()]);
        $employeesRepository->create(['name' => 'Employee B', 'salary' => 40000, 'department_id' => $departmentA->getId()]);

        $employees = $employeesRepository->getAll();

        self::assertCount(2, $employees);
    }

}
